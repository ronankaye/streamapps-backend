package com.streamapps.devicemanager.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.streamapps.devicemanager.models.Code;
import com.streamapps.devicemanager.services.CodeHandlerService;

@RestController
@RequestMapping("/code")
public class CodeController {

	@Autowired
	CodeHandlerService codeHandlerService;

	@GetMapping("/requestcode")
	public Code requestCode() {
		return codeHandlerService.generateUniqueCode();
	}
}
