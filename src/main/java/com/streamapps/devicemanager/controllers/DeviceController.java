package com.streamapps.devicemanager.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.streamapps.devicemanager.auth.Device;
import com.streamapps.devicemanager.contentGenerator.Content;
import com.streamapps.devicemanager.contentGenerator.ContentGenerator;
import com.streamapps.devicemanager.repository.DeviceRepository;

@RestController
@RequestMapping("/device")
public class DeviceController {

	@Autowired
	DeviceRepository deviceRepository;

	@GetMapping("/get/content/{deviceId}")
	public ResponseEntity<Content> requestCode(@PathVariable String deviceId) {

		Device deviceFetched = deviceRepository.findByDeviceId(deviceId);
		if(deviceFetched == null)
			return new ResponseEntity<Content>(new Content("N/A","N/A","N/A"), HttpStatus.NOT_FOUND);

		ContentGenerator contentGeneratorFetched = deviceFetched.getAssignedContentGenerator();
		if(contentGeneratorFetched == null)
			return new ResponseEntity<Content>(new Content("N/A", "N/A", "No content generator assigned."), HttpStatus.OK);

		Content content = contentGeneratorFetched.getContent();
		deviceFetched.setLastFetchedContent(content);
		deviceRepository.saveAndFlush(deviceFetched);

		return new ResponseEntity<Content>(content, HttpStatus.OK);
	}
}
