package com.streamapps.devicemanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.streamapps.devicemanager.auth.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Integer> {
	public User findByUserName(String username);
}
