package com.streamapps.devicemanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.streamapps.devicemanager.contentGenerator.ContentGenerator;

@Repository("contentGeneratorRepository")
public interface ContentGeneratorRepository extends JpaRepository<ContentGenerator, Integer> {
}
