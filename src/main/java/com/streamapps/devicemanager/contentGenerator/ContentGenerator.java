package com.streamapps.devicemanager.contentGenerator;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.github.twitch4j.TwitchClient;

@Entity
@Table(name = "content_generators")
@Inheritance(strategy = InheritanceType.JOINED)
public class ContentGenerator implements Serializable {

	private static final long serialVersionUID = 6197370098889171193L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "type")
	private String type;

	@OneToOne(cascade = {CascadeType.ALL})
	private Content content;

	@Column(name = "displayName")
	private String displayName;

	public ContentGenerator() {
		setType("generic");
		setDisplayName("default");
	}

	protected void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	protected void setContent(Content content) {
		this.content = content;
	}

	public Content getContent() {
		return this.content;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Content generateContent(TwitchClient twitchClient) {
		Content generatedContent = new Content("Default_User", "Default_Stat_Name", "Default_Stat_Value");
		this.setContent(generatedContent);
		return generatedContent;
	}
}
