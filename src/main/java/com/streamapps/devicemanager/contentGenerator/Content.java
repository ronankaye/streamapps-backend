package com.streamapps.devicemanager.contentGenerator;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contents")
public class Content implements Serializable {

	private static final long serialVersionUID = -3227901773143385958L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name= "username")
	private String username;

	@Column(name= "statName")
	private String statName;

	@Column(name= "statValue")
	private String statValue;

	public Content() {
		this.username = "";
		this.statName = "";
		this.statValue = "";
	}

	public Content(String username, String statName, String statValue) {
		this.username = username;
		this.statName = statName;
		this.statValue = statValue;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStatName() {
		return statName;
	}

	public void setStatName(String statName) {
		this.statName = statName;
	}

	public String getStatValue() {
		return statValue;
	}

	public void setStatValue(String statValue) {
		this.statValue = statValue;
	}

	public String toString() {
		return getUsername() + ";" + getStatName() + ";" + getStatValue();
	}
}
