package com.streamapps.devicemanager.services;

public interface ContentGeneratorExecutorService {

	/**
	 * Starts the executor service.
	 */
	public void start();

	/**
	 * Stops the executor service.
	 */
	public void stop();
}
